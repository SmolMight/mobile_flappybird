﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PipeSpawner : MonoBehaviour
{
    private const float CAMERA_ORTHO_SIZE = 50f;
    private const float PIPE_WIDTH = 10.4f;
    private const float PIPE_HEAD_HEIGHT = 5.625f;

    private void Start(){
        CreatePipe(25.0f, 0.0f, true);
        CreatePipe(25.0f, 0.0f, false);
    }

    /// <summary>
    /// Pipe Spawning system
    /// </summary>
    /// <param name="height">height of the pipe's body</param>
    /// <param name="xPosition">starting x position of the entire pipe</param>
    /// <param name="isBottomUp">checks if pipe is oriented BottomUp or TopBottom</param>
    private void CreatePipe(float height, float xPosition, bool isBottomUp){
        // Pipe Head
        Transform pipeHead = Instantiate(GameAssets.GetInstance().PipeHead);
        float pipeHeadYPos;

        if (isBottomUp) {
            pipeHeadYPos = -CAMERA_ORTHO_SIZE + height - PIPE_HEAD_HEIGHT;
        }else {
            pipeHeadYPos = CAMERA_ORTHO_SIZE - height;
        }

        pipeHead.position = new Vector3(xPosition, pipeHeadYPos);


        // Pipe Body
        Transform pipeBody = Instantiate(GameAssets.GetInstance().PipeBody);
        float pipeBodyYPos;
        Vector3 pipeOrientation;

        pipeOrientation = (isBottomUp) ? new Vector3(1.0f, 1.0f, 1.0f) : new Vector3(1.0f, -1.0f, 1.0f);
        pipeBodyYPos = (isBottomUp) ? -CAMERA_ORTHO_SIZE : CAMERA_ORTHO_SIZE;
        pipeBody.position = new Vector3(xPosition, pipeBodyYPos);
        pipeBody.localScale = pipeOrientation;

        SpriteRenderer sr_PipeBody = pipeBody.GetComponent<SpriteRenderer>();
        sr_PipeBody.size = new Vector2(PIPE_WIDTH, height);

        BoxCollider2D col_PipeBody = pipeBody.GetComponent<BoxCollider2D>();
        col_PipeBody.offset = new Vector2(0.0f, sr_PipeBody.size.y / 2.0f);
        col_PipeBody.size = sr_PipeBody.size;
    }
}
