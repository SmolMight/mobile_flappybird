﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdMovement : MonoBehaviour
{
    private Rigidbody2D rig2D;
    private const float JUMP_AMOUNT = 100f;

    private void Awake()
    {
        rig2D = GetComponent<Rigidbody2D>();   
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space) || Input.GetMouseButtonDown(0))
        {
            Jump();
        }
    }

    private void Jump()
    {
        rig2D.velocity = Vector2.up * JUMP_AMOUNT;
    }
}
